dnl $Id$
dnl config.m4 for extension fetch_url

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(fetch_url, for fetch_url support,
dnl Make sure that the comment is aligned:
dnl [  --with-fetch_url             Include fetch_url support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(fetch_url, whether to enable fetch_url support,
dnl Make sure that the comment is aligned:
[  --enable-fetch_url           Enable fetch_url support])

SEARCH_PATH="/usr/local/include /usr/include"
LIB_SEARCH_PATH="/usr/lib /usr/local/lib"

if test "$PHP_FETCH_URL" != "no"; then
  dnl 验证curl头文件
  for i in $SEARCH_PATH ; do
    if test -f $i/curl/curl.h; then
      CURL_DIR=$i
    fi
  done

  if test -z "$CURL_DIR"; then
    AC_MSG_ERROR([curl.h not found])
  fi


  dnl 验证libcurl.so文件
  for i in $LIB_SEARCH_PATH ; do
    if test -f $i/libcurl.so; then
      LIB_CURL_DIR=$i
    fi
  done

  if test -z "$LIB_CURL_DIR"; then
    AC_MSG_ERROR([libcurl.so not found])
  fi

  PHP_ADD_INCLUDE($CURL_DIR)
  PHP_ADD_LIBRARY_WITH_PATH(curl, $LIB_CURL_DIR, FETCH_URL_SHARED_LIBADD)

  PHP_NEW_EXTENSION(fetch_url, fetch_url.c, $ext_shared)
fi