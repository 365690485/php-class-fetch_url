<?php
// class FetchUrl{
// 	function __construct();

// 	//返回网页内容 常用于fetch()方法返回false时
// 	function body();

// 	//将对象的数据重新初始化,用于多次重用一个FetchUrl对象
// 	function clean();

// 	//返回错误信息
// 	function errmsg();

// 	//返回错误码,>0表示有错误
// 	function errcode();

// 	/**
// 	* 发起请求
// 	* $url string 请求地址
// 	* $callback function 匿名函数
// 	*/
// 	function fetch(string $url, function $callback);

// 	//请求返回HTTP Code
// 	function httpCode();

// 	//请求返回Cookies数组
// 	function responseCookies();

// 	//请求返回头部信息数组
// 	function responseHeaders();

// 	//是否允许截断，默认为不允许
// 	function setAllowRedirect(bool $allow=false);

// 	//设置连接超时时间
// 	function setConnectTimeout(int $seconds=5);

// 	//在发起的请求中,添加cookie数据
// 	function setCookie(string $name, string $value);

// 	//在发起的请求中,批量添加cookie数据
// 	function setCookies(array $cookies);

// 	//设置请求的方法(POST/GET)
// 	function setMethod(string $method="get");

// 	//设置POST方法的数据
// 	function setPostData(array $data);

// 	//设置读取超时时间
// 	function setReadTimeout(int $seconds=60);

// 	function __destroy();
// }

/*GET抓取http://www.baidu.com*/
/*
$fetch_url = new FetchUrl();
$fetch_url->setAllowRedirect(true);
$fetch_url->fetch('http://www.baidu.com');
*/

$cookies = array(
	'wei_xin_wb_session'=>'value',
	'wei_xin_wxblog_authcoder'=>'value');

/*POST提交数据*/
/*
$fetch_url = new FetchUrl();
$fetch_url->setMethod('post');
$data  = array(
	'step'=>2,
	'pays[1]'=>0,
	'pays[2]'=>0,
	'pays[3]'=>0
);
$fetch_url->setCookies($cookies);
$fetch_url->setPostData($data);
$fetch_url->fetch('http://test.wx.pp.cc/wb_advs/manage?inajax=1');
*/

//POST上传数据和文件
$fetch_url = new FetchUrl();
$fetch_url->setAllowRedirect(true);
$fetch_url->setMethod('post');
$data = array(
	'nickname'=>'挺好a',
	'wxnickname'=>'good',
	'wxusername'=>'good',
	'intro'=>'good'
);

$fetch_url->setCookies($cookies);
$fetch_url->setPostData($data);
$binary = file_get_contents("http://www.baidu.com/img/shouye_b5486898c692066bd2cbaeda86d74448.gif");
$fetch_url->setBinary("picfile", "demo.jpg", $binary);//上传二进制文件
// $fetch_url->setFile("picfile", "C:/Users/Administrator/Desktop/123.jpg");//上传指定文件

if($fetch_url->errcode() == 0){
	$fetch_url->fetch('http://wx.pp.cc/wb_ajax/addwxuser/0');
	if($fetch_url->httpCode() == 200){
		$html = $fetch_url->body();
		echo $html;
	}
}else{
	echo "errmsg:".$fetch_url->errmsg().", errcode:".$fetch_url->errcode();
}

//返回请求头部信息
print_r($fetch_url->responseHeaders());

//清空之前的请求设置，复用$fetch_url。
$fetch_url->clean();


$fetch_url->fetch("http://www.baidu.com");
print_r($fetch_url->responseHeaders());