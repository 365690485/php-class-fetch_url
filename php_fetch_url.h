/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2012 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_FETCH_URL_H
#define PHP_FETCH_URL_H

extern zend_module_entry fetch_url_module_entry;
#define phpext_fetch_url_ptr &fetch_url_module_entry

#ifdef PHP_WIN32
# define PHP_FETCH_URL_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
# define PHP_FETCH_URL_API __attribute__ ((visibility("default")))
#else
# define PHP_FETCH_URL_API
#endif

#ifdef PHP_WIN32
  #define FETCH_CURL_MODE CURL_GLOBAL_WIN32
#else
  #define FETCH_CURL_MODE CURL_GLOBAL_ALL
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

#define FETCH_CLASS_NAME  "FetchUrl"
#define FETCH_CLASS_CE    g_fetch_ce
#define FETCH_THIS        Z_OBJCE_P(getThis()), getThis()
#define FETCH_ERROR(errmsg, errno) zend_update_property_stringl(FETCH_THIS, ZEND_STRL("errmsg"), errmsg, sizeof(errmsg)-1 TSRMLS_CC);\
                                    zend_update_property_long(FETCH_THIS, ZEND_STRL("errno"), errno TSRMLS_CC)

PHP_MINIT_FUNCTION(fetch_url);
PHP_MSHUTDOWN_FUNCTION(fetch_url);
PHP_RINIT_FUNCTION(fetch_url);
PHP_RSHUTDOWN_FUNCTION(fetch_url);
PHP_MINFO_FUNCTION(fetch_url);

#ifdef ZTS
#define FETCH_URL_G(v) TSRMG(fetch_url_globals_id, zend_fetch_url_globals *, v)
#else
#define FETCH_URL_G(v) (fetch_url_globals.v)
#endif

#endif  /* PHP_FETCH_URL_H */